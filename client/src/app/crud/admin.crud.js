import axios from "axios";

const CancelToken = axios.CancelToken;
let source = null;

const BASE_URL = process.env.REACT_APP_NODE_ENV === 'development' ? process.env.REACT_APP_PUBLIC_URL : window.location.origin;


// KOMITENTI
const CREATE_KOMITENT = `${BASE_URL}/admin/createKomitent`;
const GET_KOMITENT_BY_ID = `${BASE_URL}/admin/getKomitentById`;
const KOMITENTI = `${BASE_URL}/admin/komitenti`;
const CHANGE_KOMITENT_PROFILE = `${BASE_URL}/admin/changeKomitentProfile`;
const DELETE_KOMITENTI = `${BASE_URL}/admin/deleteKomitenti`;
const PRASHALNICI_CHARTS = `${BASE_URL}/admin/prashalniciCharts`;

// GET PRASHALNICI
const GET_PRASHALNIK_MART_BY_ID = `${BASE_URL}/admin/getPrasalnikMartById`;
const GET_PRASHALNIK_MAJ_BY_ID = `${BASE_URL}/admin/getPrasalnikMajById`;
const GET_PRASHALNIK_JUNI_BY_ID = `${BASE_URL}/admin/getPrasalnikJuniById`;
const GET_PRASHALNIK_AVGUST_BY_ID = `${BASE_URL}/admin/getPrasalnikAvgustById`;

// TEHNICHARI
const GET_ALL_TEHNICHARI = `${BASE_URL}/admin/getTehnichari`;
const ADD_KOMITENTI_TO_TEHNICHARI = `${BASE_URL}/admin/addKomitentiToTehnichari`;
const DELETE_TEHNICHARI = `${BASE_URL}/admin/deleteTehnichari`;
const USER_BY_ID_URL = `${BASE_URL}/admin/getTehnicharById`;
const GET_KOMITENTI_BY_TEHNICHAR_ID = `${BASE_URL}/admin/getKomitentiByTehnicharId`;
const REMOVE_KOMITENTI_FROM_TEHNICHAR = `${BASE_URL}/admin/removeKomitentiFromtehnichar`;
const CHANGE_USERS_PROFILE = `${BASE_URL}/admin/changeTehnicharProfile`;
const CREATE_PROFILE_URL = `${BASE_URL}/admin/createTehnichar`;


// KOMITENTI
export function getKomitenti() {
    return axios.get(KOMITENTI);
}

export function deleteKomitenti(users) {
    return axios.post(DELETE_KOMITENTI, users);
}

export function createKomitent(profile) {
    return axios.post(CREATE_KOMITENT, profile);
}

export function getKomitentById(profile) {
    return axios.post(GET_KOMITENT_BY_ID, profile);
}

export function changeKomitentiProfile(profile) {
    return axios.put(CHANGE_KOMITENT_PROFILE, profile);
}
export function prashalniciCharts() {
    source = CancelToken.source();
    return axios.get(PRASHALNICI_CHARTS, { cancelToken: source.token });
}

export function cancelPrashalniciCharts() {
    // cancel the request (the message parameter is optional)
    source.cancel('Operation canceled by the user.');
}

// GET PRASHALNICI
export function getPrasalnikMartById(profile) {
    return axios.post(GET_PRASHALNIK_MART_BY_ID, profile);
}

export function getPrasalnikMajById(profile) {
    return axios.post(GET_PRASHALNIK_MAJ_BY_ID, profile);
}

export function getPrasalnikJuniById(profile) {
    return axios.post(GET_PRASHALNIK_JUNI_BY_ID, profile);
}

export function getPrasalnikAvgustById(profile) {
    return axios.post(GET_PRASHALNIK_AVGUST_BY_ID, profile);
}

// TEHNICHARI
export function getAllTehnichari() {
    return axios.get(GET_ALL_TEHNICHARI);
}

export function addKomitentiToTehnichari(model) {
    return axios.post(ADD_KOMITENTI_TO_TEHNICHARI, model);
}

export function deleteTehnichari(users) {
    return axios.post(DELETE_TEHNICHARI, users);
}

export function getKomitentiByTehnicharId(model) {
    return axios.post(GET_KOMITENTI_BY_TEHNICHAR_ID, model);
}

export function removeKomitentiFromtehnichar(model) {
    return axios.post(REMOVE_KOMITENTI_FROM_TEHNICHAR, model);
}

export function changeTehnicharProfile(profile) {
    return axios.put(CHANGE_USERS_PROFILE, profile);
}

export function getTehnicharById(profile) {
    return axios.post(USER_BY_ID_URL, profile);
}

export function createTehnichar(profile) {
    return axios.post(CREATE_PROFILE_URL, profile);
}