import axios from "axios";

const CancelToken = axios.CancelToken;
let source = null;

const BASE_URL = process.env.REACT_APP_NODE_ENV === 'development' ? process.env.REACT_APP_PUBLIC_URL : window.location.origin;

// USERS
const USERS = `${BASE_URL}/superadmin/users`;
const CREATE_PROFILE_URL = `${BASE_URL}/superadmin/register`;
const USER_BY_ID_URL = `${BASE_URL}/superadmin/user`;
const CHANGE_USERS_PROFILE = `${BASE_URL}/superadmin/changeUsersProfile`;
const DELETE_USERS = `${BASE_URL}/superadmin/deleteUsers`;

// KOMITENTI
const KOMITENTI = `${BASE_URL}/superadmin/komitenti`;
const CREATE_KOMITENT = `${BASE_URL}/superadmin/createKomitent`;
const GET_KOMITENT_BY_ID = `${BASE_URL}/superadmin/getKomitentById`;
const CHANGE_KOMITENT_PROFILE = `${BASE_URL}/superadmin/changeKomitentProfile`;
const DELETE_KOMITENTI = `${BASE_URL}/superadmin/deleteKomitenti`;
const PRASHALNICI_CHARTS = `${BASE_URL}/superadmin/prashalniciCharts`;

// GET PRASHALNICI
const GET_PRASHALNIK_MART_BY_ID = `${BASE_URL}/superadmin/getPrasalnikMartById`;
const GET_PRASHALNIK_MAJ_BY_ID = `${BASE_URL}/superadmin/getPrasalnikMajById`;
const GET_PRASHALNIK_JUNI_BY_ID = `${BASE_URL}/superadmin/getPrasalnikJuniById`;
const GET_PRASHALNIK_AVGUST_BY_ID = `${BASE_URL}/superadmin/getPrasalnikAvgustById`;

// CHANGE PRASHALNICI
const CHANGE_PRASHALNIK_MART_BY_ID = `${BASE_URL}/superadmin/changePrasalnikMartById`;
const CHANGE_PRASHALNIK_MAJ_BY_ID = `${BASE_URL}/superadmin/changePrasalnikMajById`;
const CHANGE_PRASHALNIK_JUNI_BY_ID = `${BASE_URL}/superadmin/changePrasalnikJuniById`;
const CHANGE_PRASHALNIK_AVGUST_BY_ID = `${BASE_URL}/superadmin/changePrasalnikAvgustById`;

// TEHNICHARI
const GET_ALL_TEHNICHARI = `${BASE_URL}/superadmin/getTehnichari`;
const ADD_KOMITENTI_TO_TEHNICHARI = `${BASE_URL}/superadmin/addKomitentiToTehnichari`;
const GET_KOMITENTI_BY_TEHNICHAR_ID = `${BASE_URL}/superadmin/getKomitentiByTehnicharId`;
const REMOVE_KOMITENTI_FROM_TEHNICHAR = `${BASE_URL}/superadmin/removeKomitentiFromtehnichar`;

// users
export function createProfile(profile) {
    return axios.post(CREATE_PROFILE_URL, profile);
}

export function getUserById(profile) {
    return axios.post(USER_BY_ID_URL, profile);
}

export function changeUsersProfile(profile) {
    return axios.put(CHANGE_USERS_PROFILE, profile);
}

export function getUsers() {
    return axios.get(USERS);
}

export function deleteUsers(users) {
    return axios.post(DELETE_USERS, users);
}

/// Komitenti

export function getKomitenti() {
    return axios.get(KOMITENTI);
}

export function deleteKomitenti(users) {
    return axios.post(DELETE_KOMITENTI, users);
}

export function createKomitent(profile) {
    return axios.post(CREATE_KOMITENT, profile);
}

export function getKomitentById(profile) {
    return axios.post(GET_KOMITENT_BY_ID, profile);
}

export function changeKomitentiProfile(profile) {
    return axios.put(CHANGE_KOMITENT_PROFILE, profile);
}

// GET PRASHALNICI
export function getPrasalnikMartById(profile) {
    return axios.post(GET_PRASHALNIK_MART_BY_ID, profile);
}

export function getPrasalnikMajById(profile) {
    return axios.post(GET_PRASHALNIK_MAJ_BY_ID, profile);
}

export function getPrasalnikJuniById(profile) {
    return axios.post(GET_PRASHALNIK_JUNI_BY_ID, profile);
}

export function getPrasalnikAvgustById(profile) {
    return axios.post(GET_PRASHALNIK_AVGUST_BY_ID, profile);
}

//PUT prashalnici
export function changePrasalnikMartById(profile) {
    return axios.put(CHANGE_PRASHALNIK_MART_BY_ID, profile);
}

export function changePrasalnikMajById(profile) {
    return axios.put(CHANGE_PRASHALNIK_MAJ_BY_ID, profile);
}

export function changePrasalnikJuniById(profile) {
    return axios.put(CHANGE_PRASHALNIK_JUNI_BY_ID, profile);
}

export function changePrasalnikAvgustById(profile) {
    return axios.put(CHANGE_PRASHALNIK_AVGUST_BY_ID, profile);
}

export function getAllTehnichari() {
    return axios.get(GET_ALL_TEHNICHARI);
}

export function addKomitentiToTehnichari(model) {
    return axios.post(ADD_KOMITENTI_TO_TEHNICHARI, model);
}

export function getKomitentiByTehnicharId(model) {
    return axios.post(GET_KOMITENTI_BY_TEHNICHAR_ID, model);
}

export function removeKomitentiFromtehnichar(model) {
    return axios.post(REMOVE_KOMITENTI_FROM_TEHNICHAR, model);
}

export function prashalniciCharts() {
    source = CancelToken.source();
    return axios.get(PRASHALNICI_CHARTS, { cancelToken: source.token });
}

export function cancelPrashalniciCharts() {
    // cancel the request (the message parameter is optional)
    source.cancel('Operation canceled by the user.');
}
