import axios from "axios";

const BASE_URL = process.env.REACT_APP_NODE_ENV === 'development' ? process.env.REACT_APP_PUBLIC_URL : window.location.origin;

// public
export const LOGIN_URL = `${BASE_URL}/public/login`;
export const REQUEST_PASSWORD_URL = `${BASE_URL}/public/forgotPassword`;
export const REQUEST_RESET_PASSWORD_URL = `${BASE_URL}/public/resetPassword`;

export function login(email, password) {
  return axios.post(LOGIN_URL, {
    email, password, webId: '2ee67dab-91eb-4dac-88ae-97f59596cb72'
  });
}

export function requestPassword(email) {
  return axios.post(REQUEST_PASSWORD_URL, { email });
}

export function requestResetPassword(password) {
  return axios.post(REQUEST_RESET_PASSWORD_URL, password);
}