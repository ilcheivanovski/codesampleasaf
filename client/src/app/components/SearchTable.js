/* eslint-disable no-restricted-imports */
import React from "react";
import { TextField } from "@material-ui/core";

export default function SearchTable(props) {
    const [selectedFilter, setSelectedFilter] = React.useState('');
    let inputText = React.useRef('');

    const {
        filters,
        setRows,
        searchedRows,
    } = props;

    const handleFilterSelectChange = async (e) => {
        const value = e.target.value;
        await setTimeout(() => {
            setSelectedFilter(value);
        }, 220)
    };
    const handleFilterInputChange = async (e) => {
        const value = e.target.value;
        await setTimeout(() => {
            inputText.current = value;
        }, 220)
    };

    const handleOnSearchClick = () => {
        if (inputText.hasOwnProperty('current') && inputText.current && selectedFilter) {
            setRows(searchedRows.filter(
                r => r[selectedFilter] && r[selectedFilter].toLowerCase().includes(inputText.current.toLowerCase()))
            );
        }
        else {
            setRows(searchedRows);
        }
    }

    const handleOnKeyPress = (e) => {
        if (e && e.key === 'Enter') {
            handleOnSearchClick();
        }
    }
    return (
        <div className="search-container">
            <TextField
                InputLabelProps={{ shrink: true }}
                name="role"
                id="filled-select-currency-native"
                select
                label="Пребарување"
                SelectProps={{
                    native: true,
                }}
                margin="normal"
                className="kt-width-full search-input"
                onChange={handleFilterSelectChange}
                value={selectedFilter}
            >
                {
                    filters.map(option => (
                        <option key={option.value} value={option.value}>
                            {option.label}
                        </option>
                    ))
                }
            </TextField>
            {
                selectedFilter === 'UserRole'
                    ? <TextField
                        InputLabelProps={{ shrink: true }}
                        name="role"
                        id="filled-select-currency-native"
                        select
                        SelectProps={{ native: true }}
                        margin="normal"
                        className="kt-width-full search-input"
                        onChange={handleFilterInputChange}
                        value={inputText}
                    >
                        {
                            filters.map(option => (
                                <option key={option.value} value={option.value}>
                                    {option.label}
                                </option>
                            ))
                        }
                    </TextField>
                    : <TextField
                        InputLabelProps={{ shrink: true }}
                        type="text"
                        margin="normal"
                        className="kt-width-full search-input"
                        name={`${selectedFilter}`}
                        onChange={handleFilterInputChange}
                        onKeyPress={handleOnKeyPress}
                    />
            }
            <button
                className="btn btn-primary btn-elevate kt-login__btn-primary search-button"
                onClick={async () => handleOnSearchClick()}
            >
                Барај
        </button>
        </div>

    )
}