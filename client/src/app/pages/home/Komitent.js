import React, { useState, useEffect } from "react";
import { Formik } from "formik";
import { connect } from "react-redux";
import { TextField, Breadcrumbs } from "@material-ui/core";
import clsx from "clsx";

import { Link } from "react-router-dom";
import * as auth from "../../store/ducks/auth.duck";

import {
    getKomitentById as getKomitentByIdAdmin,
    changeKomitentiProfile as changeKomitentiProfileAdmin,
    getAllTehnichari as getAllTehnichariAdmin,
} from "../../crud/admin.crud";
import {
    getKomitentById as getKomitentByIdSuperadmin,
    changeKomitentiProfile as changeKomitentiProfileSuperadmin,
    getAllTehnichari as getAllTehnichariSuperadmin,
} from "../../crud/superadmin.crud";

function Komitent(props) {

    const [tehnichari, setTehnichari] = useState([{ value: '', label: 'Одбери' }]);
    const [loading, setLoading] = useState(false);
    const [initUser, setInitUser] = useState({});
    const [loadingButtonStyle, setLoadingButtonStyle] = useState({
        paddingRight: "2.5rem"
    });

    const enableLoading = () => {
        setLoading(true);
        setLoadingButtonStyle({ paddingRight: "3.5rem" });
    };

    const disableLoading = () => {
        setLoading(false);
        setLoadingButtonStyle({ paddingRight: "2.5rem" });
    };

    const param = Number(props.match.params.komitentId);

    const userModel = {
        fullname: '',
        address: '',
        contractNumber: '',
        reonski: '',
        povrsina: '',
        dogovorenaKolicina: '',
        martPrasalnikId: '',
        majPrasalnikId: '',
        juniPrasalnikId: '',
        avgustPrasalnikId: '',
    }

    const getTehnichari = async () => {
        setLoading(true);

        const alltehnichari = props.user.UserRole.roleId === 3
            ? await getAllTehnichariSuperadmin()
            : await getAllTehnichariAdmin();

        const tehnichari = alltehnichari.data.tehnichari.reduce((acc, el) => {
            const tehnichar = {
                value: el.id,
                label: `${el.firstName} ${el.lastName}`
            }
            acc.push(tehnichar)
            return acc;
        }, [{ value: '', label: 'Одбери' }]);

        setTehnichari(tehnichari);
        setLoading(false);
    }

    useEffect(() => {
        (async () => {
            await getTehnichari();

            const user = props.user.UserRole.roleId === 3
                ? await getKomitentByIdSuperadmin({ id: param })
                : await getKomitentByIdAdmin({ id: param });

            userModel.fullname = user.data.komitent.fullname;
            userModel.address = user.data.komitent.address;
            userModel.contractNumber = user.data.komitent.contractNumber;
            userModel.reonski = Number(user.data.komitent.reonski);
            userModel.povrsina = user.data.komitent.povrsina;
            userModel.dogovorenaKolicina = user.data.komitent.dogovorenaKolicina;

            userModel.martPrasalnikId = user.data.komitent.Komitent_Prasalnik_Mart.prasalnikId;
            userModel.majPrasalnikId = user.data.komitent.Komitent_Prasalnik_Maj.prasalnikId;
            userModel.juniPrasalnikId = user.data.komitent.Komitent_Prasalnik_Juni.prasalnikId;
            userModel.avgustPrasalnikId = user.data.komitent.Komitent_Prasalnik_Avgust.prasalnikId;

            setInitUser(userModel);
        })();
    }, {});

    return (
        <div className="kt-portlet">
            <div className="kt-portlet__head">
                <Breadcrumbs aria-label="Breadcrumb" style={{ marginTop: '17px', fontSize: '18px' }}>
                    <Link color="inherit" to="/komitenti" >
                        Комитенти
                    </Link>
                    <div color="textPrimary">
                        Комитент
                    </div>
                </Breadcrumbs>
            </div>

            <div className='kt-portlet__body'>

                <Formik
                    style={{ backgroundColor: 'white' }}
                    initialValues={{ ...initUser, id: param }}
                    enableReinitialize={true}
                    validate={values => {
                        const errors = {};

                        if (!values.fullname) {
                            errors.fullname = 'Полето е задолжително';
                        } else if (!values.address) {
                            errors.address = 'Полето е задолжително';
                        } else if (!values.contractNumber) {
                            errors.contractNumber = 'Полето е задолжително';
                        } else if (!values.reonski) {
                            errors.reonski = 'Полето е задолжително';
                        } else if (!values.povrsina) {
                            errors.povrsina = 'Полето е задолжително';
                        } else if (!values.dogovorenaKolicina) {
                            errors.dogovorenaKolicina = 'Полето е задолжително';
                        }

                        return errors;
                    }}
                    onSubmit={
                        (values, { setStatus, setSubmitting }) => {
                            enableLoading();
                            setSubmitting(true);

                            const promise = props.user.UserRole.roleId === 3
                                ? changeKomitentiProfileSuperadmin(values)
                                : changeKomitentiProfileAdmin(values);
                            promise
                                .then((result) => {

                                    disableLoading();
                                    setSubmitting(false);
                                    setStatus('');

                                    props.history.push('/komitenti');
                                })
                                .catch((error) => {
                                    console.log({ error })
                                    disableLoading();
                                    setSubmitting(false);
                                    setStatus('Something went wrong!');
                                });
                        }
                    }
                >
                    {({
                        values,
                        status,
                        errors,
                        touched,
                        handleChange,
                        handleBlur,
                        handleSubmit,
                        isSubmitting,
                    }) => (
                            <form onSubmit={handleSubmit} className="kt-form">
                                {status && (
                                    <div role="alert" className="alert alert-danger">
                                        <div className="alert-text">{status}</div>
                                    </div>
                                )}

                                <div className="form-group">
                                    <TextField
                                        InputLabelProps={{ shrink: true }}
                                        type="text"
                                        label="Име и презиме"
                                        margin="normal"
                                        fullWidth={true}
                                        name="fullname"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.fullname}
                                        helperText={touched.fullname && errors.fullname}
                                        error={Boolean(touched.fullname && errors.fullname)}
                                    />
                                </div>

                                <div className="form-group">
                                    <TextField
                                        InputLabelProps={{ shrink: true }}
                                        type="text"
                                        label="Адреса"
                                        margin="normal"
                                        fullWidth={true}
                                        name="address"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.address}
                                        helperText={touched.address && errors.address}
                                        error={Boolean(touched.address && errors.address)}
                                    />
                                </div>

                                <div className="form-group">
                                    <TextField
                                        InputLabelProps={{ shrink: true }}
                                        type="text"
                                        label="Број на договор"
                                        margin="normal"
                                        fullWidth={true}
                                        name="contractNumber"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.contractNumber}
                                        helperText={touched.contractNumber && errors.contractNumber}
                                        error={Boolean(touched.contractNumber && errors.contractNumber)}
                                    />
                                </div>

                                <div className="form-group">
                                    <TextField
                                        InputLabelProps={{ shrink: true }}
                                        name="reonski"
                                        id="filled-select-currency-native"
                                        select
                                        label="Реонски"
                                        SelectProps={{ native: true }}
                                        margin="normal"
                                        className="kt-width-full"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.reonski}
                                        disabled
                                        helperText={touched.reonski && errors.reonski}
                                        error={Boolean(touched.reonski && errors.reonski)}
                                    >
                                        {
                                            tehnichari.map(option => (
                                                <option key={option.value} value={option.value}>
                                                    {option.label}
                                                </option>
                                            ))
                                        }
                                    </TextField>
                                </div>

                                <div className="form-group">
                                    <TextField
                                        InputLabelProps={{ shrink: true }}
                                        type="text"
                                        margin="normal"
                                        label="Површина"
                                        className="kt-width-full"
                                        name="povrsina"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.povrsina}
                                        helperText={touched.povrsina && errors.povrsina}
                                        error={Boolean(
                                            touched.povrsina && errors.povrsina
                                        )}
                                    />
                                </div>

                                <div className="form-group">
                                    <TextField
                                        InputLabelProps={{ shrink: true }}
                                        type="text"
                                        margin="normal"
                                        label="Договорена колоичина"
                                        className="kt-width-full"
                                        name="dogovorenaKolicina"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.dogovorenaKolicina}
                                        helperText={touched.dogovorenaKolicina && errors.dogovorenaKolicina}
                                        error={Boolean(
                                            touched.dogovorenaKolicina && errors.dogovorenaKolicina
                                        )}
                                    />
                                </div>

                                <div className="form-group">
                                    <Link to={`/komitent/${param}/martPrasalnik/${values.martPrasalnikId}`}>
                                        <button type="button" className="btn btn-primary btn-elevate kt-login__btn-primary ">
                                            Март прашалник
                                        </button>
                                    </Link>
                                </div>

                                <div className="form-group">
                                    <Link to={`/komitent/${param}/majPrasalnik/${values.majPrasalnikId}`}>
                                        <button type="button" className="btn btn-primary btn-elevate kt-login__btn-primary ">
                                            Maj прашалник
                                        </button>
                                    </Link>
                                </div>

                                <div className="form-group">
                                    <Link to={`/komitent/${param}/juniPrasalnik/${values.juniPrasalnikId}`}>
                                        <button type="button" className="btn btn-primary btn-elevate kt-login__btn-primary ">
                                            Јуни прашалник
                                        </button>
                                    </Link>
                                </div>

                                <div className="form-group">
                                    <Link to={`/komitent/${param}/avgustPrasalnik/${values.avgustPrasalnikId}`}>
                                        <button type="button" className="btn btn-primary btn-elevate kt-login__btn-primary ">
                                            Август прашалник
                                        </button>
                                    </Link>
                                </div>

                                <div className="kt-login__actions">
                                    <Link to="/dashboard">
                                        <button
                                            type="button"
                                            className="btn btn-secondary btn-elevate kt-login__btn-secondary"
                                        >
                                            Назад
                                            </button>
                                    </Link>

                                    <button
                                        type="submit"
                                        style={{ marginLeft: '10px' }}
                                        className={
                                            `btn btn-primary btn-elevate kt-login__btn-primary ${clsx(
                                                {
                                                    "kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light": loading
                                                }
                                            )}`
                                        }
                                        disabled={isSubmitting}
                                    >
                                        Поднеси
                                    </button>
                                </div>
                            </form>
                        )}
                </Formik>
            </div>

        </div >
    );
}

const mapStateToProps = ({ auth: { user } }) => ({
    user
});

export default connect(mapStateToProps, auth.actions)(Komitent);