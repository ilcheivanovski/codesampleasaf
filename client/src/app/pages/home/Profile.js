import React, { useState, useEffect } from "react";
import { Formik } from "formik";
import { connect } from "react-redux";
import { TextField } from "@material-ui/core";
import clsx from "clsx";

import { Link } from "react-router-dom";
import * as auth from "../../store/ducks/auth.duck";
import { changeProfile } from "../../crud/user.crud";


function Profile(props) {

    const [loading, setLoading] = useState(false);
    const [loadingButtonStyle, setLoadingButtonStyle] = useState({
        paddingRight: "2.5rem"
    });

    const enableLoading = () => {
        setLoading(true);
        setLoadingButtonStyle({ paddingRight: "3.5rem" });
    };

    const disableLoading = () => {
        setLoading(false);
        setLoadingButtonStyle({ paddingRight: "2.5rem" });
    };

    return (
        <div className="kt-portlet">
            <div className="kt-portlet__head">
                <h3 style={{ marginTop: '17px' }}>
                    Промена на профилот
                 </h3>
            </div>

            <div className='kt-portlet__body'>

                <Formik
                    style={{ backgroundColor: 'white' }}
                    initialValues={{
                        email: props.user.email,
                        firstName: props.user.firstName,
                        lastName: props.user.lastName,
                    }}
                    onSubmit={
                        (values, { setStatus, setSubmitting }) => {
                            enableLoading();
                            setSubmitting(true);
                            changeProfile(values)
                                .then((result) => {
                                    props.fulfillUser(result.data)
                                    disableLoading();
                                    setSubmitting(false);
                                    setStatus('');

                                })
                                .catch((error) => {
                                    disableLoading();
                                    setSubmitting(false);
                                    setStatus('Something went wrong!');
                                })
                        }
                    }
                >
                    {({
                        values,
                        status,
                        errors,
                        touched,
                        handleChange,
                        handleBlur,
                        handleSubmit,
                        isSubmitting,
                    }) => (
                            <form onSubmit={handleSubmit} className="kt-form">
                                {status && (
                                    <div role="alert" className="alert alert-danger">
                                        <div className="alert-text">{status}</div>
                                    </div>
                                )}

                                <div className="form-group">
                                    <TextField
                                        type="email"
                                        label="Email"
                                        margin="normal"
                                        fullWidth={true}
                                        name="email"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.email}
                                        helperText={touched.email && errors.email}
                                        error={Boolean(touched.email && errors.email)}
                                    />
                                </div>

                                <div className="form-group">
                                    <TextField
                                        type="firstName"
                                        label="Име"
                                        margin="normal"
                                        fullWidth={true}
                                        name="firstName"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.firstName}
                                        helperText={touched.firstName && errors.firstName}
                                        error={Boolean(touched.firstName && errors.firstName)}
                                    />
                                </div>

                                <div className="form-group">
                                    <TextField
                                        type="lastName"
                                        label="Презиме"
                                        margin="normal"
                                        fullWidth={true}
                                        name="lastName"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.lastName}
                                        helperText={touched.lastName && errors.lastName}
                                        error={Boolean(touched.lastName && errors.lastName)}
                                    />
                                </div>

                                <div className="form-group">
                                    <TextField
                                        type="password"
                                        label="Лозинка"
                                        margin="normal"
                                        fullWidth={true}
                                        name="password"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.password}
                                        helperText={touched.password && errors.password}
                                        error={Boolean(touched.password && errors.password)}
                                    />
                                </div>

                                <div className="kt-login__actions">
                                    <Link to="/dashboard">
                                        <button
                                            type="button"
                                            className="btn btn-secondary btn-elevate kt-login__btn-secondary"
                                        >
                                            Назад
                                            </button>
                                    </Link>

                                    <button
                                        type="submit"
                                        style={{ marginLeft: '10px' }}
                                        className={
                                            `btn btn-primary btn-elevate kt-login__btn-primary ${clsx(
                                                {
                                                    "kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light": loading
                                                }
                                            )}`
                                        }
                                        disabled={isSubmitting}
                                    >
                                        Поднеси
                                        </button>
                                </div>
                            </form>
                        )}
                </Formik>
            </div>

        </div>
    );
}

const mapStateToProps = ({ auth: { user } }) => ({
    user
});

export default connect(mapStateToProps, auth.actions)(Profile);