import React, { useState, useEffect } from "react";
import { Formik } from "formik";
import { connect } from "react-redux";
import { TextField, Breadcrumbs } from "@material-ui/core";
import clsx from "clsx";

import { Link } from "react-router-dom";
import * as auth from "../../../store/ducks/auth.duck";
import { getPrasalnikAvgustById as getPrasalnikAvgustByIdAdmin } from "../../../crud/admin.crud";
import { getPrasalnikAvgustById as getPrasalnikAvgustByIdSuperadmin, changePrasalnikAvgustById } from "../../../crud/superadmin.crud";

function EditAvgustPrasalnik(props) {

    const [loading, setLoading] = useState(false);
    const [initPrashalnik, setInitPrashalnik] = useState({});
    const [loadingButtonStyle, setLoadingButtonStyle] = useState({
        paddingRight: "2.5rem"
    });

    const enableLoading = () => {
        setLoading(true);
        setLoadingButtonStyle({ paddingRight: "3.5rem" });
    };

    const disableLoading = () => {
        setLoading(false);
        setLoadingButtonStyle({ paddingRight: "2.5rem" });
    };

    const prasalnikId = Number(props.match.params.prasalnikId);

    let avgustPrasalnikModel = {
        dms_farmer_br_alp1: '',
        dms_full_name_al1: '',
        dms_adresa_al1: '',
        dms_tehnicar_al1: '',
        dms_dog_povrs_al1: '',
        dms_tut_pro_or_kg: '',
        dms_kof_avg_tip_poseta_ind: '',
        dms_kof_avg_tip_poseta_grp: '',
        dms_kof_avg_detska_sila: '',
        dms_kof_avg_prihod_casa: '',
        dms_kof_avg_fer_tretman: '',
        dms_kof_avg_prinudna_rab: '',
        dms_kof_avg_bez_sredina: '',
        dms_kof_avg_slob_zdruzuvanje: '',
        dms_kof_avg_soglasno_zakon: '',
        dms_fam_12_sobiranje: '',
        dms_fam_13_14_sobiranje: '',
        dms_fam_15_17_sobiranje: '',
        dms_fam_12_klasiranje: '',
        dms_fam_13_14_klasiranje: '',
        dms_fam_15_17_klasiranje: '',
        dms_fam_12_baliranje: '',
        dms_fam_13_14_baliranje: '',
        dms_fam_15_17_baliranje: '',
        dms_arg_12_sobiranje: '',
        dms_arg_13_14_sobiranje: '',
        dms_arg_15_17_sobiranje: '',
        dms_arg_12_klasiranje: '',
        dms_arg_13_14_klasiranje: '',
        dms_arg_15_17_klasiranje: '',
        dms_arg_12_baliranje: '',
        dms_arg_13_14_baliranje: '',
        dms_arg_15_17_baliranje: '',
        dms_mig_12_sobiranje: '',
        dms_mig_13_14_sobiranje: '',
        dms_mig_15_17_sobiranje: '',
        dms_mig_12_klasiranje: '',
        dms_mig_13_14_klasiranje: '',
        dms_mig_15_17_klasiranje: '',
        dms_mig_12_baliranje: '',
        dms_mig_13_14_baliranje: '',
        dms_mig_15_17_baliranje: '',
        dms_kof_avg_dat_treta_poseta: '',
        dms_rano_cutenje_vo_procenti: '',
        dms_dovolno_sushnici_dekar_gap: '',
        dms_metalni1: '',
        dms_metalni2: '',
        dms_metalni3: '',
        dms_metalninad3: '',
        dms_drveni1: '',
        dms_drveni2: '',
        dms_drveni3: '',
        dms_drveninad3: '',
        dms_betonski1: '',
        dms_betonski2: '',
        dms_betonski3: '',
        dms_betonskinad3: '',
        dms_ramki1: '',
        dms_ramki2: '',
        dms_ramki3: '',
        dms_ramkinad3: '',
        dms_berba_pocetok1_gap: '',
        dms_berba_pocetok2_gap: '',
        dms_berba_pocetok3_gap: '',
        dms_berba_pocetok4_gap: '',
        dms_berba_pocetok5_gap: '',
        dms_berba_pocetok6_gap: '',
        dms_berba_kraj1_gap: '',
        dms_berba_kraj2_gap: '',
        dms_berba_kraj3_gap: '',
        dms_berba_kraj4_gap: '',
        dms_berba_kraj5_gap: '',
        dms_berba_kraj6_gap: '',
        dms_vizuelna_los1: '',
        dms_vizuelna_sreden1: '',
        dms_vizuelna_dobar1: '',
        dms_vizuelna_odlicen1: '',
        dms_vizuelna_los2: '',
        dms_vizuelna_sreden2: '',
        dms_vizuelna_dobar2: '',
        dms_vizuelna_odlicen2: '',
        dms_vizuelna_los3: '',
        dms_vizuelna_sreden3: '',
        dms_vizuelna_dobar3: '',
        dms_vizuelna_odlicen3: '',
        dms_vizuelna_los4: '',
        dms_vizuelna_sreden4: '',
        dms_vizuelna_dobar4: '',
        dms_vizuelna_odlicen4: '',
        dms_vizuelna_los5: '',
        dms_vizuelna_sreden5: '',
        dms_vizuelna_dobar5: '',
        dms_vizuelna_odlicen5: '',
        dms_vizuelna_los6: '',
        dms_vizuelna_sreden6: '',
        dms_vizuelna_dobar6: '',
        dms_vizuelna_odlicen6: '',
        dms_nizenje_racno: '',
        dms_nizenje_masinski_so_bodenje: '',
        dms_nizenje_masinski_so_sienje: '',
        dms_isusen_tutun_juni_bali: '',
        dms_isusen_tutun_juni_kg: '',
        dms_isusen_tutun_juli_bali: '',
        dms_isusen_tutun_juli_kg: '',
        dms_isusen_tutun_avgust_bali: '',
        dms_isusen_tutun_avgust_kg: '',
        dms_isusen_tutun_septemvri_bali: '',
        dms_isusen_tutun_septemvri_kg: '',
        dms_isusen_tutun_oktomvri_bali: '',
        dms_isusen_tutun_oktomvri_kg: '',
        dms_isusen_tutun_vkupno_bali: '',
        dms_isusen_tutun_vkupno_kg: '',
        dms_prostor_dali_dovolno: '',
        dms_higiena_avgust: '',
        dms_strani_mirisi: '',
        dms_zemjodelski_krediti: '',
        dms_elementari_nepogodi: '',
        dms_osigurano_proizvodstvo: '',
        dms_zaoruvanje_na_strnishte: '',
        dms_netutunski_cisto: '',
        dms_netutunski_zagadeno_bez_opasnost: '',
        dms_zagadeno: '',
        dadeni_instrukcii_avgust: '',
    }

    useEffect(() => {
        (async () => {
            const prashalnik = props.user.UserRole.roleId === 3 ? await getPrasalnikAvgustByIdSuperadmin({ id: prasalnikId }) : await getPrasalnikAvgustByIdAdmin({ id: prasalnikId });

            avgustPrasalnikModel = prashalnik.data.prasalnik;
            avgustPrasalnikModel.potpis = avgustPrasalnikModel.potpisImage;

            delete avgustPrasalnikModel.potpisImage;

            setInitPrashalnik(avgustPrasalnikModel);
        })();
    }, {});

    const PrashalnkikMarFields = ({
        values,
        status,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
    }) => {
        let fields = [];
        for (const key in avgustPrasalnikModel) {
            fields.push(<div className="form-group">
                <TextField
                    InputLabelProps={{ shrink: true }}
                    type="text"
                    label={`${key}`}
                    margin="normal"
                    fullWidth={true}
                    name={`${key}`}
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values[key]}
                    helperText={touched[key] && errors[key]}
                    error={Boolean(touched[key] && errors[key])}
                />
            </div>);
        }
        return fields;
    }

    return (
        <div className="kt-portlet">
            <div className="kt-portlet__head">
                <Breadcrumbs aria-label="Breadcrumb" style={{ marginTop: '17px', fontSize: '18px' }}>
                    <Link color="inherit" to="/users" >
                        Комитенти
                    </Link>
                    <Link color="inherit" to={`/komitent/${props.match.params.komitentId}`} >
                        Комитент
                    </Link>
                    <div color="textPrimary">
                        Едитирање на прашалник Август
                    </div>
                </Breadcrumbs>
            </div>

            <div className='kt-portlet__body'>

                <Formik
                    style={{ backgroundColor: 'white' }}
                    initialValues={{ ...initPrashalnik, id: prasalnikId }}
                    enableReinitialize={true}
                    onSubmit={
                        (values, { setStatus, setSubmitting }) => {
                            enableLoading();
                            setSubmitting(true);
                            changePrasalnikAvgustById(values)
                                .then((result) => {

                                    disableLoading();
                                    setSubmitting(false);
                                    setStatus('');

                                    props.history.push('/komitenti');
                                })
                                .catch((error) => {
                                    console.log({ error })
                                    disableLoading();
                                    setSubmitting(false);
                                    setStatus('Something went wrong!');
                                });
                        }
                    }
                >
                    {({
                        values,
                        status,
                        errors,
                        touched,
                        handleChange,
                        handleBlur,
                        handleSubmit,
                        isSubmitting,
                    }) => (
                            <form onSubmit={handleSubmit} className="kt-form">
                                {status && (
                                    <div role="alert" className="alert alert-danger">
                                        <div className="alert-text">{status}</div>
                                    </div>
                                )}

                                {
                                    PrashalnkikMarFields({
                                        values,
                                        status,
                                        errors,
                                        touched,
                                        handleChange,
                                        handleBlur,
                                        handleSubmit,
                                        isSubmitting,
                                    }).map((e) => {
                                        return e
                                    })
                                }

                                <div style={{ display: 'flex', flexDirection: 'column', marginBottom: '20px' }}>
                                    <label>Потпис</label>
                                    <img width="200" src={initPrashalnik.potpis} />
                                </div>

                                <div className="kt-login__actions">
                                    <Link to="/dashboard">
                                        <button
                                            type="button"
                                            className="btn btn-secondary btn-elevate kt-login__btn-secondary"
                                        >
                                            Назад
                                        </button>
                                    </Link>

                                    <button
                                        type="submit"
                                        style={{ marginLeft: '10px' }}
                                        className={
                                            `btn btn-primary btn-elevate kt-login__btn-primary ${clsx(
                                                {
                                                    "kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light": loading
                                                }
                                            )}`
                                        }
                                        disabled={isSubmitting}
                                    >
                                        Поднеси
                                        </button>
                                </div>
                            </form>
                        )}
                </Formik>
            </div>

        </div >
    );
}

const mapStateToProps = ({ auth: { user } }) => ({
    user
});

export default connect(mapStateToProps, auth.actions)(EditAvgustPrasalnik);