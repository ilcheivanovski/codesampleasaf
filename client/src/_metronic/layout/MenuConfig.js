export default {// delete local storage after change
  aside: {
    self: {},
    items: [
      {
        title: "Dashboard",
        root: true,
        icon: "flaticon2-architecture-and-city",
        page: "dashboard",
        translate: "MENU.DASHBOARD",
        bullet: "dot",
        roles: [3]
      },

      { section: "Техничари", roles: [2] },
      {
        title: "Техничари",
        root: true,
        icon: "flaticon-users",
        page: "users",
        bullet: "dot",
        roles: [2]
      },
      {
        title: "Креирај техничар",
        root: true,
        icon: "flaticon-user-add",
        page: "createUser",
        bullet: "dot",
        roles: [2]
      },
      { section: "Техничари/Админи", roles: [3] },
      {
        title: "Техничари/Админи",
        root: true,
        icon: "flaticon-users",
        page: "users",
        bullet: "dot",
        roles: [3]
      },
      {
        title: "Креирај техничар/админ",
        root: true,
        icon: "flaticon-user-add",
        page: "createUser",
        bullet: "dot",
        roles: [3]
      },
      { section: "Комитенти", roles: [2, 3] },
      {
        title: "Комитенти",
        root: true,
        icon: "flaticon-users",
        page: "komitenti",
        bullet: "dot",
        roles: [2, 3]
      },
      {
        title: "Креирај Комитент",
        root: true,
        icon: "flaticon-user-add",
        page: "createKomitent",
        bullet: "dot",
        roles: [2, 3]
      },
    ]
  }
};
