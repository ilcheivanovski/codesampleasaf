import React from "react";
import MenuSection from "./MenuSection";
import MenuItemSeparator from "./MenuItemSeparator";
import MenuItem from "./MenuItem";
import { connect } from "react-redux";

// case 1: {
//   return 'user';
// }
// case 2: {
//   return 'admin';
// }
// case 3: {
//   return 'superadmin';
// }

class MenuList extends React.Component {
  render() {
    const { currentUrl, menuConfig, layoutConfig, user } = this.props;

    return menuConfig.aside.items.map((child, index) => {
      return (
        <React.Fragment key={`menuList${index}`}>
          {
            child.section && user && child.roles.includes(user.UserRole.roleId) && <MenuSection item={child} />
          }
          {child.separator && user && child.roles.includes(user.UserRole.roleId) && <MenuItemSeparator item={child} />}
          {
            child.title && user && child.roles.includes(user.UserRole.roleId) && (
              <MenuItem
                item={child}
                currentUrl={currentUrl}
                layoutConfig={layoutConfig}
              />
            )
          }
        </React.Fragment>
      );
    });
  }
}

const mapStateToProps = ({ auth: { user } }) => ({
  user
});

export default connect(mapStateToProps)(MenuList);