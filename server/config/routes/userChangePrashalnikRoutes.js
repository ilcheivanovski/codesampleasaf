const userChangePrashalnikRoutes = {
    'PUT /changePrasalnikMartById': 'KomitentController.changePrasalnikMartById',
    'PUT /changePrasalnikMajById': 'KomitentController.changePrasalnikMajById',
    'PUT /changePrasalnikJuniById': 'KomitentController.changePrasalnikJuniById',
    'PUT /changePrasalnikAvgustById': 'KomitentController.changePrasalnikAvgustById',
};

module.exports = userChangePrashalnikRoutes;