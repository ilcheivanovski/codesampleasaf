const superadminRoutes = {
  'GET /users': 'UserController.getAll',
  'POST /register': 'UserController.register',
  'POST /user': 'UserController.getUserById',
  'PUT /changeUsersProfile': 'UserController.changeUsersProfile',
  'POST /deleteUsers': 'UserController.deleteUsers',

  'GET /komitenti': 'KomitentController.komitenti',
  'POST /createKomitent': 'KomitentController.createKomitent',
  'POST /getKomitentById': 'KomitentController.getKomitentById',
  'PUT /changeKomitentProfile': 'KomitentController.changeKomitentProfile',
  'POST /deleteKomitenti': 'KomitentController.deleteKomitenti',

  'POST /getPrasalnikMartById': 'KomitentController.getPrasalnikMartById',
  'POST /getPrasalnikMajById': 'KomitentController.getPrasalnikMajById',
  'POST /getPrasalnikJuniById': 'KomitentController.getPrasalnikJuniById',
  'POST /getPrasalnikAvgustById': 'KomitentController.getPrasalnikAvgustById',

  'PUT /changePrasalnikMartById': 'KomitentController.changePrasalnikMartById',
  'PUT /changePrasalnikMajById': 'KomitentController.changePrasalnikMajById',
  'PUT /changePrasalnikJuniById': 'KomitentController.changePrasalnikJuniById',
  'PUT /changePrasalnikAvgustById': 'KomitentController.changePrasalnikAvgustById',

  'GET /getTehnichari': 'KomitentController.getTehnichari',
  'POST /addKomitentiToTehnichari': 'KomitentController.addKomitentiToTehnichari',
  'POST /getKomitentiByTehnicharId': 'KomitentController.getKomitentiByTehnicharId',
  'POST /removeKomitentiFromtehnichar': 'KomitentController.removeKomitentiFromtehnichar',

  'GET /prashalniciCharts': 'KomitentController.prashalniciCharts',
};

module.exports = superadminRoutes;
