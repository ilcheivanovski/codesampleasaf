const publicRoutes = {
  'POST /login': 'UserController.login',
  'POST /forgotPassword': 'UserController.forgotPassword',
  'POST /resetPassword': 'UserController.resetPassword',
};

module.exports = publicRoutes;