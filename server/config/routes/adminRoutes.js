const adminRoutes = {

  'POST /createKomitent': 'KomitentController.createKomitent',
  'POST /getKomitentById': 'KomitentController.getKomitentById',
  'GET /komitenti': 'KomitentController.komitenti',
  'PUT /changeKomitentProfile': 'KomitentController.changeKomitentProfile',
  'POST /deleteKomitenti': 'KomitentController.deleteKomitenti',

  'GET /getTehnichari': 'KomitentController.getTehnichari',
  'POST /addKomitentiToTehnichari': 'KomitentController.addKomitentiToTehnichari',
  'POST /getKomitentiByTehnicharId': 'KomitentController.getKomitentiByTehnicharId',
  'POST /removeKomitentiFromtehnichar': 'KomitentController.removeKomitentiFromtehnichar',
  'GET /prashalniciCharts': 'KomitentController.prashalniciCharts',

  'POST /getTehnicharById': 'UserController.getTehnicharById',
  'POST /deleteTehnichari': 'UserController.deleteTehnichari',
  'PUT /changeTehnicharProfile': 'UserController.changeTehnicharProfile',
  'POST /createTehnichar': 'UserController.createTehnichar',

  'POST /getPrasalnikMartById': 'KomitentController.getPrasalnikMartById',
  'POST /getPrasalnikMajById': 'KomitentController.getPrasalnikMajById',
  'POST /getPrasalnikJuniById': 'KomitentController.getPrasalnikJuniById',
  'POST /getPrasalnikAvgustById': 'KomitentController.getPrasalnikAvgustById',

};

module.exports = adminRoutes;