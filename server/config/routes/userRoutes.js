const userRoutes = {
  'PUT /changeProfile': 'UserController.changeProfile',
  
  'GET /getKomitentiMobile': 'KomitentController.getKomitentiMobile',

  'POST /getPrasalnikMartById': 'KomitentController.getPrasalnikMartById',
  'POST /getPrasalnikMajById': 'KomitentController.getPrasalnikMajById',
  'POST /getPrasalnikJuniById': 'KomitentController.getPrasalnikJuniById',
  'POST /getPrasalnikAvgustById': 'KomitentController.getPrasalnikAvgustById',
};

module.exports = userRoutes;