const Sequelize = require('sequelize');
const sequelize = require('../../config/database');

const hooks = {};
const tableName = 'Komitent_Prasalnik_Majs';

const Komitent_Prasalnik_Maj = sequelize.define('Komitent_Prasalnik_Maj', {
  komitentId: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
  prasalnikId: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
}, { hooks, tableName });

// eslint-disable-next-line
Komitent_Prasalnik_Maj.prototype.toJSON = function () {
  const values = Object.assign({}, this.get());

  return values;
};

module.exports = Komitent_Prasalnik_Maj;
