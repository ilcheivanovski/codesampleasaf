const Sequelize = require('sequelize');
const sequelize = require('../../config/database');

const hooks = {};
const tableName = 'userRoles';

const UserRole = sequelize.define('UserRole', {
    userId: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    roleId: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
}, {
    hooks,
    tableName,
    timestamps: false,
    charset: 'utf8',
    collate: 'utf8_unicode_ci'
});

// eslint-disable-next-line
UserRole.prototype.toJSON = function () {
    const values = Object.assign({}, this.get());

    return values;
};

module.exports = UserRole;
