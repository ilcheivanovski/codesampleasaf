const Sequelize = require('sequelize');
const sequelize = require('../../config/database');

const hooks = {};
const tableName = 'Prasalnik_zelena_taksacijas';

const Prasalnik_zelena_taksacija = sequelize.define('Prasalnik_zelena_taksacija', {
  dms_kolkuKg_soTKP: {
    type: Sequelize.STRING,
  },
  dms_kolkuKg_visok: {
    type: Sequelize.STRING,
  },
  dms_kolkuKg_vkupno: {
    type: Sequelize.STRING,
  },
}, { hooks, tableName });

// eslint-disable-next-line
Prasalnik_zelena_taksacija.prototype.toJSON = function () {
  const values = Object.assign({}, this.get());

  return values;
};

module.exports = Prasalnik_zelena_taksacija;
