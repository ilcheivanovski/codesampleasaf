const Sequelize = require('sequelize');

const sequelize = require('../../config/database');

//r models
const Komitent_Prasalnik_Mart = require('../models/Komitent_Prasalnik_Mart');
const Komitent_Prasalnik_Maj = require('../models/Komitent_Prasalnik_Maj');
const Komitent_Prasalnik_Juni = require('../models/Komitent_Prasalnik_Juni');
const Komitent_Prasalnik_Avgust = require('../models/Komitent_Prasalnik_Avgust');

const hooks = {};

const tableName = 'Komitents';

const Komitent = sequelize.define('Komitent', {
    fullname: {
        type: Sequelize.STRING,
    },
    address: {
        type: Sequelize.STRING,
    },
    contractNumber: {
        type: Sequelize.STRING,
        unique: true,
    },
    reonski: {
        type: Sequelize.STRING,
    },
    povrsina: {
        type: Sequelize.STRING,
    },
    dogovorenaKolicina: {
        type: Sequelize.STRING,
    },
}, { hooks, tableName });

// Associations
Komitent.hasOne(Komitent_Prasalnik_Mart, { foreignKey: 'komitentId' });
Komitent.hasOne(Komitent_Prasalnik_Maj, { foreignKey: 'komitentId' });
Komitent.hasOne(Komitent_Prasalnik_Juni, { foreignKey: 'komitentId' });
Komitent.hasOne(Komitent_Prasalnik_Avgust, { foreignKey: 'komitentId' });


// eslint-disable-next-line
Komitent.prototype.toJSON = function () {
    const values = Object.assign({}, this.get());

    return values;
};

module.exports = Komitent;
