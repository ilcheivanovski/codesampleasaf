/**
 * third party libraries
 */
const bodyParser = require('body-parser');
const express = require('express');
const helmet = require('helmet');
const http = require('http');
const mapRoutes = require('express-routes-mapper');
const cors = require('cors');
const path = require('path');

/**
 * server configuration
 */
const config = require('../config/');
const dbService = require('./services/db.service');

const user = require('./policies/user.policy');
const superAdmin = require('./policies/superadmin.policy');
const admin = require('./policies/admin.policy');

// upload
const multer = require('multer')
const upload = multer();

// environment: development, staging, testing, production
const environment = process.env.NODE_ENV;

/**
 * express application
 */
const app = express();
const server = http.Server(app);

// rotues
const mappedOpenRoutes = mapRoutes(config.publicRoutes, 'api/controllers/');
const mappedSuperAdminRoutes = mapRoutes(config.superadminRoutes, 'api/controllers/');
const mappedAdminRoutes = mapRoutes(config.adminRoutes, 'api/controllers/');
const mappedUserRoutes = mapRoutes(config.userRoutes, 'api/controllers/');
const mappedUserChangePrashalnikRoutes = mapRoutes(config.userChangePrashalnikRoutes, 'api/controllers/', [upload.single('potpis')]);

const DB = dbService(environment, config.migrate).start();

// allow cross origin requests
// configure to only allow requests from certain origins
app.use(cors());

// secure express app
app.use(helmet({
  dnsPrefetchControl: false,
  frameguard: false,
  ieNoOpen: false,
}));

// parsing the request bodys
app.use(bodyParser.urlencoded({ extended: false }, { limit: '10mb' }));
app.use(bodyParser.json({ limit: '10mb' }));

// secure your private routes with jwt authentication middleware
app.all('/user/*', (req, res, next) => user(req, res, next));
app.all('/admin/*', (req, res, next) => admin(req, res, next));
app.all('/superadmin/*', (req, res, next) => superAdmin(req, res, next));

// fill routes for express application
app.use('/public', mappedOpenRoutes);
app.use('/user', mappedUserRoutes);
app.use('/user', mappedUserChangePrashalnikRoutes);

app.use('/admin', mappedAdminRoutes);
app.use('/superadmin', mappedSuperAdminRoutes);


if (environment === 'production') {
  console.log('[INFO]____PRODUCTION_____[INFO]')
  app.use(express.static(path.join(__dirname, './../../client/build')));

  //serve the index.html file for each route - angular
  app.get('/*', function (req, res) {
    res.sendFile(path.join(__dirname, './../../client/build/index.html'));
  });
}

server.listen(config.port, () => {
  if (environment !== 'production' &&
    environment !== 'development' &&
    environment !== 'testing'
  ) {
    console.error(`NODE_ENV is set to ${environment}, but only production and development are valid.`);
    process.exit(1);
  }
  return DB;
});
