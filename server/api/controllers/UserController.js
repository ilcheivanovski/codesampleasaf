const User = require('../models/User');
const Role = require('../models/Role');
const UserRole = require('../models/UserRole');

const authService = require('../services/auth.service');
const bcryptService = require('../services/bcrypt.service');
const sgMail = require('@sendgrid/mail');
const environment = process.env.NODE_ENV;

const UserController = () => {
  const register = async (req, res) => {
    const { body } = req;

    if (body.password1 === body.password2) {
      try {

        const existingEmail = await User.findOne({
          where: {
            email: body.email
          }
        });

        if (existingEmail) {
          return res.status(400).json({ msg: 'Внесениот email постои' });
        }

        const user = await User.create({
          email: body.email,
          password: body.password1,
          firstName: body.firstName,
          lastName: body.lastName,
        });
        const token = authService().issue({ id: user.id });

        const userRole = await Role.findOne({
          where: {
            name: 'user'
          }
        });

        const adminRole = await Role.findOne({
          where: {
            name: 'admin'
          }
        });

        if (body.role === 'user') {
          await UserRole.create({
            userId: user.id,
            roleId: userRole.id
          });
        }

        if (body.role === 'admin') {
          await UserRole.create({
            userId: user.id,
            roleId: adminRole.id
          });
        }

        return res.status(200).json({ token, user });
      } catch (err) {
        console.log(err);
        return res.status(500).json({ msg: 'Грешка' });
      }
    }

    return res.status(400).json({ msg: 'Лозинките не се совпаѓаат' });
  };

  const createTehnichar = async (req, res) => {
    const { body } = req;

    if (body.password1 === body.password2) {
      try {

        if (body.role !== 'user') {
          return res.status(401).json({ msg: 'Unauthorized' });
        }

        const existingEmail = await User.findOne({
          where: {
            email: body.email
          }
        });

        if (existingEmail) {
          return res.status(400).json({ msg: 'Внесениот email постои' });
        }

        const user = await User.create({
          email: body.email,
          password: body.password1,
          firstName: body.firstName,
          lastName: body.lastName,
        });
        const token = authService().issue({ id: user.id });

        await UserRole.create({
          userId: user.id,
          roleId: 1,
        });

        return res.status(200).json({ token, user });
      } catch (err) {
        console.log(err);
        return res.status(500).json({ msg: 'Грешка' });
      }
    }

    return res.status(400).json({ msg: 'Лозинките не се совпаѓаат' });
  };

  const getUserById = async (req, res) => {
    try {
      const user = await User.findOne({
        where: {
          id: req.body.id,
        },
        attributes: ['id', 'firstName', 'lastName', 'email'],
        include: [{
          model: UserRole,
          required: true,
        }]
      });

      return res.status(200).json({ user });
    } catch (err) {
      console.log(err);
      return res.status(500).json({ msg: 'Internal server error' });
    }
  }

  const getTehnicharById = async (req, res) => {
    try {


      const userRoles = await UserRole.findOne({
        where: {
          userId: req.body.id,
        },
      });

      if (userRoles.roleId !== 1) {
        return res.status(401).json({ msg: 'Unauthorized' });
      }

      const user = await User.findOne({
        where: {
          id: req.body.id,
        },
        attributes: ['id', 'firstName', 'lastName', 'email'],
        include: [{
          model: UserRole,
          required: true,
        }]
      });

      return res.status(200).json({ user });
    } catch (err) {
      console.log(err);
      return res.status(500).json({ msg: 'Internal server error' });
    }
  }

  const login = async (req, res) => {
    const { email, password, webId } = req.body;

    if (email && password) {
      try {
        const user = await User
          .findOne({
            where: {
              email,
            },
            include: [{
              model: UserRole,
              required: true,

            }],
          });

        if (!user) {
          return res.status(400).json({ msg: 'Bad Request: User not found' });
        }

        if (bcryptService().comparePassword(password, user.password)) {

          // if is tehnichar and is from web deny it
          if (user.UserRole.roleId === 1 && webId && webId === '2ee67dab-91eb-4dac-88ae-97f59596cb72') {
            return res.status(401).json({ msg: 'Unauthorized' });
          }

          const token = authService().issue({ id: user.id });

          // save token in user
          if (token) {
            user.token = token;
            user.update(user.dataValues);
          }

          return res.status(200).json({ token, user });
        }

        return res.status(401).json({ msg: 'Unauthorized' });
      } catch (err) {
        console.log(err);
        return res.status(500).json({ msg: 'Internal server error' });
      }
    }

    return res.status(400).json({ msg: 'Bad Request: Email or password is wrong' });
  };

  const validate = (req, res) => {
    const { token } = req.body;

    authService().verify(token, (err) => {
      if (err) {
        return res.status(401).json({ isvalid: false, err: 'Invalid Token!' });
      }

      return res.status(200).json({ isvalid: true });
    });
  };

  const getAll = async (req, res) => {
    try {
      const users = await User.findAll({
        include: [{
          model: UserRole,
          required: true,
        }]
      });

      return res.status(200).json({ users });
    } catch (err) {
      console.log(err);
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };

  const changeUsersProfile = async (req, res) => {
    try {

      const id = req.body.id;
      if (id) {
        const user = await User
          .findOne({
            where: {
              id,
            },
          });

        let { email, password1, firstName, lastName } = req.body;

        if (email) {
          user.email = email;
        }

        if (password1) {
          user.password = bcryptService().password({ password: password1 });
        }

        if (firstName) {
          user.firstName = firstName;
        }

        if (lastName) {
          user.lastName = lastName;
        }

        const userRole = await Role.findOne({
          where: {
            name: 'user'
          }
        });

        const adminRole = await Role.findOne({
          where: {
            name: 'admin'
          }
        });

        if (req.body.role === 'user') {
          const existingUserRole = await UserRole.findOne({
            where: {
              userId: id
            }
          });
          existingUserRole.roleId = userRole.id;

          await existingUserRole.update(existingUserRole.dataValues);
        }

        if (req.body.role === 'admin') {
          const existingUserRole = await UserRole.findOne({
            where: {
              userId: id
            }
          });

          existingUserRole.roleId = adminRole.id;

          await existingUserRole.update(existingUserRole.dataValues);
        }

        await user.update(user.dataValues);
        return res.status(200).json(user);
      }

      return res.status(500).json({ msg: 'Internal server error' });
    } catch (err) {
      console.log(err);
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };

  const changeTehnicharProfile = async (req, res) => {
    try {

      const id = req.body.id;

      if (id) {

        const existingUserRole = await UserRole.findOne({
          where: {
            userId: id
          }
        });

        if (existingUserRole.roleId !== 1) {
          return res.status(401).json({ msg: 'Unauthorized' });
        }

        const user = await User
          .findOne({
            where: {
              id,
            },
          });

        let { email, password1, firstName, lastName } = req.body;

        if (email) {
          user.email = email;
        }

        if (password1) {
          user.password = bcryptService().password({ password: password1 });
        }

        if (firstName) {
          user.firstName = firstName;
        }

        if (lastName) {
          user.lastName = lastName;
        }

        await user.update(user.dataValues);
        return res.status(200).json(user);
      }

      return res.status(500).json({ msg: 'Internal server error' });
    } catch (err) {
      console.log(err);
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };

  const changeProfile = async (req, res) => {
    try {
      const token = req.headers.authorization;
      const id = authService().getUserIdByToken(token);
      if (id) {
        const user = await User
          .findOne({
            where: {
              id,
            },
            include: [{
              model: UserRole,
              required: true,
            }]
          });

        let { email, password, firstName, lastName } = req.body;

        if (email) {
          user.email = email;
        }

        if (password) {
          user.password = bcryptService().password({ password: password });
        }

        if (firstName) {
          user.firstName = firstName;
        }

        if (lastName) {
          user.lastName = lastName;
        }

        await user.update(user.dataValues);
        return res.status(200).json(user);
      }

      return res.status(500).json({ msg: 'Internal server error' });
    } catch (err) {
      console.log(err);
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };

  const forgotPassword = async (req, res) => {

    await sgMail.setApiKey(process.env.SENDGRID_API_KEY);

    const { email } = req.body;

    if (email) {
      try {
        const user = await User
          .findOne({
            where: {
              email,
            },
          });

        if (!user) {
          return res.status(400).json({ msg: `Корисникот не постои.` });
        }

        const token = authService().issueForgot({ id: user.id });

        const host = environment === 'production' ? req.get('host') : process.env.LOCALHOST_URL;
        const protocol = req.protocol;

        const msg = {
          to: email,
          from: 'info@tkprilep.com.mk',
          subject: 'Ресетирање на лозинка.',
          html: `<p>Кликнете на линкот за да ја ресетирате лозинката.<a href="${protocol}://${host}/auth/reset-password/${token}">link</a></p>`,
        };

        await sgMail.send(msg);

        return res.status(200).json({ msg: `Линкот за ресетирање е пратен. Проверете го вазшиот email.` });
      }
      catch (err) {
        console.log(err)

        return res.status(500).json({ msg: 'Internal server error' });
      }
    }

    return res.status(400).json({ msg: `Email-от е потребен.` });
  };

  const resetPassword = async (req, res) => {

    const { body } = req;

    if (body.password1 === body.password2) {

      try {

        const id = await authService().resetPasswordUserId(req.body.token);

        const user = await User
          .findOne({
            where: {
              id,
            },
          });

        if (!user) {
          return res.status(500).json({ msg: `Корисникот не постои.` });
        }

        user.password = body.password1;
        const newHashedPassword = bcryptService().password(user);

        await user.update({ password: newHashedPassword });
        return res.status(200).json({ msg: `Лозинката е ресетирана.` });
      }

      catch (e) {
        console.log(e);
        return res.status(500).json({ msg: 'Internal server error' });
      }
    }

    return res.status(400).json({ msg: `Лозинките не се софпаѓаат.` });
  };

  const deleteUsers = async (req, res) => {
    try {
      const { selected } = req.body;

      await User.destroy({
        where: {
          id: selected,
        }
      });

      return res.status(200).json({ msg: 'Избришани' });
    } catch (err) {
      console.log(err);
      return res.status(500).json({ msg: 'Internal server error' });
    }
  }

  const deleteTehnichari = async (req, res) => {
    try {
      const { selected } = req.body;

      const userRoles = await UserRole.findAll({
        where: {
          roleId: 1,
          userId: selected,
        },
      });

      await User.destroy({
        where: {
          id: userRoles.map(ur => ur.userId),
        }
      });

      return res.status(200).json({ msg: 'Избришани' });
    } catch (err) {
      console.log(err);
      return res.status(500).json({ msg: 'Internal server error' });
    }
  }

  return {
    register,
    login,
    validate,
    getAll,
    changeProfile,
    forgotPassword,
    resetPassword,
    getUserById,
    getTehnicharById,
    changeUsersProfile,
    deleteUsers,
    deleteTehnichari,
    changeTehnicharProfile,
    createTehnichar,
  };

};

module.exports = UserController;
