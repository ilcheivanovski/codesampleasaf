const sequelize = require('../../config/database');
const authService = require('../services/auth.service');

const Komitent = require('../models/Komitent');
const Role = require('../models/Role');
const UserRole = require('../models/UserRole');
const User = require('../models/User');

//models
const Prasalnik_Mart = require('../models/Prasalnik_Mart');
const Prasalnik_Maj = require('../models/Prasalnik_Maj');
const Prasalnik_Juni = require('../models/Prasalnik_Juni');
const Prasalnik_Avgust = require('../models/Prasalnik_Avgust');

//r models
const Komitent_Prasalnik_Mart = require('../models/Komitent_Prasalnik_Mart');
const Komitent_Prasalnik_Maj = require('../models/Komitent_Prasalnik_Maj');
const Komitent_Prasalnik_Juni = require('../models/Komitent_Prasalnik_Juni');
const Komitent_Prasalnik_Avgust = require('../models/Komitent_Prasalnik_Avgust');

// relacija pomegju komitent i tehnichar
const TehnicharKomitent = require('../models/TehnicharKomitent');

const excludeInPercentage = ['id', 'status', 'popolneto', 'createdAt', 'updatedAt'];
const getPercentage = (dbPrasalnik) => {

    const prasalnik = { ...dbPrasalnik };
    for (const key in prasalnik) {
        if (prasalnik.hasOwnProperty(key) && excludeInPercentage.includes(key)) {
            delete prasalnik[key];
        }
    }

    const numberOfAll = Object.keys(prasalnik).length;
    const numberOfFilled = Object
        .values(prasalnik)
        .filter(e => e !== null && e !== '' && typeof e === 'string' && e.trim() !== '')
        .length;
    const percentage = numberOfFilled * 100 / numberOfAll;

    console.log({ numberOfAll, numberOfFilled, percentage });

    return percentage;
};

const KomitentController = () => {

    const createKomitent = async (req, res) => {
        const { body } = req;

        try {

            const komitentContractNumber = await Komitent.findOne({ where: { contractNumber: body.contractNumber } });

            if (komitentContractNumber) {
                return res.status(400).json({ msg: 'Внесениот број на договор постои' });
            }

            const komitent = await Komitent.create({
                fullname: body.fullname,
                address: body.address,
                contractNumber: body.contractNumber,
                reonski: Number(body.reonski),
                povrsina: body.povrsina,
                dogovorenaKolicina: body.dogovorenaKolicina,
            });

            const Prasalnik_Mart_1 = await Prasalnik_Mart.create({});
            const Prasalnik_Maj_1 = await Prasalnik_Maj.create({});
            const Prasalnik_Juni_1 = await Prasalnik_Juni.create({});
            const Prasalnik_Avgust_1 = await Prasalnik_Avgust.create({});

            await Komitent_Prasalnik_Mart.create({ komitentId: komitent.id, prasalnikId: Prasalnik_Mart_1.id });
            await Komitent_Prasalnik_Maj.create({ komitentId: komitent.id, prasalnikId: Prasalnik_Maj_1.id });
            await Komitent_Prasalnik_Juni.create({ komitentId: komitent.id, prasalnikId: Prasalnik_Juni_1.id });
            await Komitent_Prasalnik_Avgust.create({ komitentId: komitent.id, prasalnikId: Prasalnik_Avgust_1.id });

            await TehnicharKomitent.create({
                tehnicharId: Number(body.reonski),
                komitentId: komitent.id
            });

            return res.status(200).json({ komitent });
        } catch (err) {
            console.log(err);
            return res.status(500).json({ msg: 'Серверска грешка' });
        }

    };

    const getKomitentById = async (req, res) => {
        try {
            const dbkomitent = await Komitent.findOne({
                where: {
                    id: req.body.id,
                },
                attributes: ['fullname', 'address', 'contractNumber', 'reonski', 'povrsina', 'dogovorenaKolicina'],
                include: [{
                    model: Komitent_Prasalnik_Mart,
                    required: true,
                },
                {
                    model: Komitent_Prasalnik_Maj,
                    required: true,
                },
                {
                    model: Komitent_Prasalnik_Juni,
                    required: true,
                },
                {
                    model: Komitent_Prasalnik_Avgust,
                    required: true,
                }]
            });

            const prasalnik_Mart = await Prasalnik_Mart.findOne({
                where: {
                    id: dbkomitent.Komitent_Prasalnik_Mart.prasalnikId
                }
            });
            const prasalnik_Maj = await Prasalnik_Maj.findOne({
                where: {
                    id: dbkomitent.Komitent_Prasalnik_Maj.prasalnikId
                }
            });
            const prasalnik_Juni = await Prasalnik_Juni.findOne({
                where: {
                    id: dbkomitent.Komitent_Prasalnik_Juni.prasalnikId
                }
            });
            const prasalnik_Avgust = await Prasalnik_Avgust.findOne({
                where: {
                    id: dbkomitent.Komitent_Prasalnik_Avgust.prasalnikId
                }
            });

            const komitent = {
                ...dbkomitent.dataValues,
                prasalnici: {
                    prasalnik_Mart: {
                        status: prasalnik_Mart.status,
                        popolneto: prasalnik_Mart.popolneto,
                    },
                    prasalnik_Maj: {
                        status: prasalnik_Maj.status,
                        popolneto: prasalnik_Maj.popolneto,
                    },
                    prasalnik_Juni: {
                        status: prasalnik_Juni.status,
                        popolneto: prasalnik_Juni.popolneto,
                    },
                    prasalnik_Avgust: {
                        status: prasalnik_Avgust.status,
                        popolneto: prasalnik_Avgust.popolneto,
                    }
                }
            };

            return res.status(200).json({ komitent });
        } catch (err) {
            console.log(err);
            return res.status(500).json({ msg: 'Internal server error' });
        }
    }

    const getTehnichari = async (req, res) => {
        try {

            const tehnicharRole = await Role.findOne({
                where: {
                    name: 'user'
                }
            });

            const userRoles = await UserRole.findAll({
                where: {
                    roleId: tehnicharRole.id,
                },
            });

            const tehnichariRoleIds = userRoles.map(r => r.userId)

            const tehnichari = await User.findAll({
                where: {
                    id: tehnichariRoleIds,
                },
                include: [{
                    model: UserRole,
                    required: true,
                }]
            });

            return res.status(200).json({ tehnichari });
        } catch (err) {
            console.log(err);
            return res.status(500).json({ msg: 'Internal server error' });
        }
    }
    /// GET

    const getPrasalnikMartById = async (req, res) => {
        try {
            const prasalnik = await Prasalnik_Mart.findOne({
                where: {
                    id: req.body.id,
                },
            });

            prasalnik.potpisImage = prasalnik.potpisImage
                ? `data:${prasalnik.potpisImageType};base64, ${new Buffer(prasalnik.potpisImage, 'binary').toString('base64')}`
                : null;

            return res.status(200).json({ prasalnik });
        } catch (err) {
            console.log(err);
            return res.status(500).json({ msg: 'Internal server error' });
        }
    }

    const getPrasalnikMajById = async (req, res) => {
        try {
            const prasalnik = await Prasalnik_Maj.findOne({
                where: {
                    id: req.body.id,
                },
            });

            prasalnik.potpisImage = prasalnik.potpisImage
                ? `data:${prasalnik.potpisImageType};base64, ${new Buffer(prasalnik.potpisImage, 'binary').toString('base64')}`
                : null;

            return res.status(200).json({ prasalnik });
        } catch (err) {
            console.log(err);
            return res.status(500).json({ msg: 'Internal server error' });
        }
    }

    const getPrasalnikJuniById = async (req, res) => {
        try {
            const prasalnik = await Prasalnik_Juni.findOne({
                where: {
                    id: req.body.id,
                },
            });

            prasalnik.potpisImage = prasalnik.potpisImage
                ? `data:${prasalnik.potpisImageType};base64, ${new Buffer(prasalnik.potpisImage, 'binary').toString('base64')}`
                : null;

            return res.status(200).json({ prasalnik });
        } catch (err) {
            console.log(err);
            return res.status(500).json({ msg: 'Internal server error' });
        }
    }

    const getPrasalnikAvgustById = async (req, res) => {
        try {
            const prasalnik = await Prasalnik_Avgust.findOne({
                where: {
                    id: req.body.id,
                },
            });

            prasalnik.potpisImage = prasalnik.potpisImage
                ? `data:${prasalnik.potpisImageType};base64, ${new Buffer(prasalnik.potpisImage, 'binary').toString('base64')}`
                : null;

            return res.status(200).json({ prasalnik });
        } catch (err) {
            console.log(err);
            return res.status(500).json({ msg: 'Internal server error' });
        }
    }

    /// PUT

    const changePrasalnikMartById = async (req, res) => {
        try {
            const { body } = req;
            const prasalnik = await Prasalnik_Mart.findOne({
                where: {
                    id: body.id,
                },
            });

            const status = Number(req.body.status);
            const currentStatus = Number(prasalnik.status);

            if (!status) {
                return res.status(404).json({ msg: 'Ве молиме ставете статус' });
            }

            if (currentStatus === 2) {
                return res.status(404).json({ msg: 'Прашалникот е затворен' });
            } else if (currentStatus === 0 && status === 1) {
                prasalnik.status = 1;
            } else if (
                (currentStatus === 0 && status === 2) ||
                (currentStatus === 1 && status === 1) ||
                (currentStatus === 1 && status === 2)
            ) {
                prasalnik.status = 2;
            } else {
                return res.status(404).json({ msg: 'Ве молиме ставете ВАЛИДЕН статус' });
            }

            Object.setPrototypeOf(body, Object.prototype);
            if (req.file) {
                prasalnik.potpisImage = req.file.buffer;
                prasalnik.potpisImageType = req.file.mimetype;
            }

            const excluded = ['status'];

            for (const key in body) {
                if (body.hasOwnProperty(key) && !excluded.includes(key)) {
                    const value = body[key];
                    if (value) {
                        prasalnik[key] = value;
                    }
                }
            }

            const popolneto = getPercentage(prasalnik.dataValues);

            prasalnik.update({ ...prasalnik.dataValues, popolneto });

            return res.status(200).json({ msg: 'Success' });
        } catch (err) {
            console.log(err);
            return res.status(500).json({ msg: 'Internal server error' });
        }
    }

    const changePrasalnikMajById = async (req, res) => {
        try {
            const { body } = req;
            const prasalnik = await Prasalnik_Maj.findOne({
                where: {
                    id: body.id,
                },
            });

            const status = Number(req.body.status);
            const currentStatus = Number(prasalnik.status);

            if (!status) {
                return res.status(404).json({ msg: 'Ве молиме ставете статус' });
            }

            if (currentStatus === 2) {
                return res.status(404).json({ msg: 'Прашалникот е затворен' });
            } else if (currentStatus === 0 && status === 1) {
                prasalnik.status = 1;
            } else if (
                (currentStatus === 0 && status === 2) ||
                (currentStatus === 1 && status === 1) ||
                (currentStatus === 1 && status === 2)
            ) {
                prasalnik.status = 2;
            } else {
                return res.status(404).json({ msg: 'Ве молиме ставете ВАЛИДЕН статус' });
            }

            Object.setPrototypeOf(body, Object.prototype);
            if (req.file) {
                prasalnik.potpisImage = req.file.buffer;
                prasalnik.potpisImageType = req.file.mimetype;
            }

            const excluded = ['status'];

            for (const key in body) {
                if (body.hasOwnProperty(key) && !excluded.includes(key)) {
                    const value = body[key];
                    if (value) {
                        prasalnik[key] = value;
                    }
                }
            }

            const popolneto = getPercentage(prasalnik.dataValues);

            prasalnik.update({ ...prasalnik.dataValues, popolneto });

            return res.status(200).json({ msg: 'Success' });
        } catch (err) {
            console.log(err);
            return res.status(500).json({ msg: 'Internal server error' });
        }
    }

    const changePrasalnikJuniById = async (req, res) => {
        try {
            const { body } = req;
            const prasalnik = await Prasalnik_Juni.findOne({
                where: {
                    id: body.id,
                },
            });

            const status = Number(req.body.status);
            const currentStatus = Number(prasalnik.status);

            if (!status) {
                return res.status(404).json({ msg: 'Ве молиме ставете статус' });
            }

            if (currentStatus === 2) {
                return res.status(404).json({ msg: 'Прашалникот е затворен' });
            } else if (currentStatus === 0 && status === 1) {
                prasalnik.status = 1;
            } else if (
                (currentStatus === 0 && status === 2) ||
                (currentStatus === 1 && status === 1) ||
                (currentStatus === 1 && status === 2)
            ) {
                prasalnik.status = 2;
            } else {
                return res.status(404).json({ msg: 'Ве молиме ставете ВАЛИДЕН статус' });
            }

            Object.setPrototypeOf(body, Object.prototype);
            if (req.file) {
                prasalnik.potpisImage = req.file.buffer;
                prasalnik.potpisImageType = req.file.mimetype;
            }

            const excluded = ['status'];

            for (const key in body) {
                if (body.hasOwnProperty(key) && !excluded.includes(key)) {
                    const value = body[key];
                    if (value) {
                        prasalnik[key] = value;
                    }
                }
            }

            const popolneto = getPercentage(prasalnik.dataValues);

            prasalnik.update({ ...prasalnik.dataValues, popolneto });

            return res.status(200).json({ msg: 'Success' });
        } catch (err) {
            console.log(err);
            return res.status(500).json({ msg: 'Internal server error' });
        }
    }

    const changePrasalnikAvgustById = async (req, res) => {
        try {
            const { body } = req;
            const prasalnik = await Prasalnik_Avgust.findOne({
                where: {
                    id: body.id,
                },
            });

            const status = Number(req.body.status);
            const currentStatus = Number(prasalnik.status);

            if (!status) {
                return res.status(404).json({ msg: 'Ве молиме ставете статус' });
            }

            if (currentStatus === 2) {
                return res.status(404).json({ msg: 'Прашалникот е затворен' });
            } else if (currentStatus === 0 && status === 1) {
                prasalnik.status = 1;
            } else if (
                (currentStatus === 0 && status === 2) ||
                (currentStatus === 1 && status === 1) ||
                (currentStatus === 1 && status === 2)
            ) {
                prasalnik.status = 2;
            } else {
                return res.status(404).json({ msg: 'Ве молиме ставете ВАЛИДЕН статус' });
            }

            Object.setPrototypeOf(body, Object.prototype);
            if (req.file) {
                prasalnik.potpisImage = req.file.buffer;
                prasalnik.potpisImageType = req.file.mimetype;
            }

            const excluded = ['status'];

            for (const key in body) {
                if (body.hasOwnProperty(key) && !excluded.includes(key)) {
                    const value = body[key];
                    if (value) {
                        prasalnik[key] = value;
                    }
                }
            }

            const popolneto = getPercentage(prasalnik.dataValues);

            prasalnik.update({ ...prasalnik.dataValues, popolneto });

            return res.status(200).json({ msg: 'Success' });
        } catch (err) {
            console.log(err);
            return res.status(500).json({ msg: 'Internal server error' });
        }
    }

    const komitenti = async (req, res) => {
        try {

            const users = await Komitent.findAll({
                include:
                    [
                        {
                            model: Komitent_Prasalnik_Mart,
                            required: true,
                        },
                        {
                            model: Komitent_Prasalnik_Maj,
                            required: true,
                        },
                        {
                            model: Komitent_Prasalnik_Juni,
                            required: true,
                        },
                        {
                            model: Komitent_Prasalnik_Avgust,
                            required: true,
                        }
                    ]
            });

            return res.status(200).json({ users });
        } catch (err) {
            console.log(err);
            return res.status(500).json({ msg: 'Internal server error' });
        }
    };

    const changeKomitentProfile = async (req, res) => {
        try {

            const id = req.body.id;
            if (id) {
                const komitent = await Komitent
                    .findOne({
                        where: {
                            id,
                        },
                    });

                let { fullname, address, contractNumber, reonski, povrsina, dogovorenaKolicina } = req.body;

                if (fullname) {
                    komitent.fullname = fullname;
                }

                if (address) {
                    komitent.address = address;
                }

                if (contractNumber) {
                    komitent.contractNumber = contractNumber;
                }

                if (reonski) {
                    komitent.reonski = reonski;
                }

                if (povrsina) {
                    komitent.povrsina = povrsina;
                }

                if (dogovorenaKolicina) {
                    komitent.dogovorenaKolicina = dogovorenaKolicina;
                }

                await komitent.update(komitent.dataValues);
                return res.status(200).json(komitent);
            }

            return res.status(500).json({ msg: 'Internal server error' });
        } catch (err) {
            console.log(err);
            return res.status(500).json({ msg: 'Internal server error' });
        }
    };

    const deleteKomitenti = async (req, res) => {
        try {
            const { selected } = req.body;

            await Komitent.destroy({
                where: {
                    id: selected,
                }
            });

            return res.status(200).json({ msg: 'Избришани' });
        } catch (err) {
            console.log(err);
            return res.status(500).json({ msg: 'Internal server error' });
        }
    }

    const addKomitentiToTehnichari = async (req, res) => {

        const { tehnichariIds, komitentiIds } = req.body;

        try {

            if (!!!tehnichariIds.length || !!!komitentiIds.length) {
                return res.status(400).json({ msg: 'Bad Request' });
            }

            await User.findOne({ where: { id: tehnichariIds[0] } })


            komitentiIds.forEach(async (komitentId) => {
                let komitent = await Komitent.findOne({ where: { id: komitentId } });
                komitent.update({ reonski: tehnichariIds[0] })
                await TehnicharKomitent.create({
                    tehnicharId: tehnichariIds[0],
                    komitentId
                });
            });

            return res.status(200).json({ msg: 'Success' });
        } catch (err) {
            console.log(err);
            return res.status(500).json({ msg: 'Internal server error' });
        }

    };

    const getKomitentiByTehnicharId = async (req, res) => {
        try {

            const { limit, currentPage } = req.query;
            const offset = limit * (currentPage - 1);

            const token = req.headers.authorization;

            if (!token) {
                return res.status(404).json({ msg: 'No token provided' });
            }

            const tehnicharId = req.body.id;

            let tehnicharKomitents = [];
            if (limit && currentPage) {
                tehnicharKomitents = await TehnicharKomitent.findAll({
                    limit: Number(limit),
                    offset: Number(offset),
                    where: {
                        tehnicharId: tehnicharId,
                    },
                });
            } else {
                tehnicharKomitents = await TehnicharKomitent.findAll({
                    where: {
                        tehnicharId: tehnicharId,
                    },
                });
            }


            const komitentsIds = tehnicharKomitents.map(t => t.komitentId);
            const komitentiDb = await Komitent.findAll({
                where: {
                    id: komitentsIds
                },
                include: [{
                    model: Komitent_Prasalnik_Mart,
                    required: true,
                },
                {
                    model: Komitent_Prasalnik_Maj,
                    required: true,
                },
                {
                    model: Komitent_Prasalnik_Juni,
                    required: true,
                },
                {
                    model: Komitent_Prasalnik_Avgust,
                    required: true,
                }]
            });


            const komitentiAsync = komitentiDb.reduce(async (acc, dbkomitent) => {

                const prasalnik_Mart = await Prasalnik_Mart.findOne({
                    where: {
                        id: dbkomitent.Komitent_Prasalnik_Mart.prasalnikId
                    }
                });
                const prasalnik_Maj = await Prasalnik_Maj.findOne({
                    where: {
                        id: dbkomitent.Komitent_Prasalnik_Maj.prasalnikId
                    }
                });
                const prasalnik_Juni = await Prasalnik_Juni.findOne({
                    where: {
                        id: dbkomitent.Komitent_Prasalnik_Juni.prasalnikId
                    }
                });
                const prasalnik_Avgust = await Prasalnik_Avgust.findOne({
                    where: {
                        id: dbkomitent.Komitent_Prasalnik_Avgust.prasalnikId
                    }
                });

                const komitent = {
                    ...dbkomitent.dataValues,
                    prasalnici: {
                        prasalnik_Mart: {
                            status: prasalnik_Mart.status,
                            popolneto: prasalnik_Mart.popolneto,
                        },
                        prasalnik_Maj: {
                            status: prasalnik_Maj.status,
                            popolneto: prasalnik_Maj.popolneto,
                        },
                        prasalnik_Juni: {
                            status: prasalnik_Juni.status,
                            popolneto: prasalnik_Juni.popolneto,
                        },
                        prasalnik_Avgust: {
                            status: prasalnik_Avgust.status,
                            popolneto: prasalnik_Avgust.popolneto,
                        }
                    }
                };
                acc = await acc;
                acc.push(komitent);

                return acc;
            }, []);

            const komitenti = await komitentiAsync;

            return res.status(200).json({ komitenti });
        } catch (err) {
            console.log(err);
            return res.status(500).json({ msg: 'Internal server error' });
        }
    }

    const getKomitentiMobile = async (req, res) => {
        try {

            const { limit, currentPage, search } = req.query;
            const offset = limit * (currentPage - 1);

            const token = req.headers.authorization;

            if (!token) {
                return res.status(404).json({ msg: 'No token provided' });
            }

            const tehnicharId = authService().getUserIdByToken(token);

            let tehnicharKomitents = [];
            if (limit && currentPage) {
                tehnicharKomitents = await TehnicharKomitent.findAll({
                    limit: Number(limit),
                    offset: Number(offset),
                    where: {
                        tehnicharId: tehnicharId,
                    },
                });
            } else {
                tehnicharKomitents = await TehnicharKomitent.findAll({
                    where: {
                        tehnicharId: tehnicharId,
                    },
                });
            }


            let komitentsIds = tehnicharKomitents.map(t => t.komitentId);

            if (search) {
                const searchedKomitenti = await sequelize.query(`
                        SELECT * FROM Komitents WHERE
                                                      id IN (${komitentsIds.toString()}) and fullname LIKE '%${search}%' or
                                                      id IN (${komitentsIds.toString()}) and address LIKE '%${search}%' or
                                                      id IN (${komitentsIds.toString()}) and contractNumber LIKE '%${search}%' or
                                                      id IN (${komitentsIds.toString()}) and reonski LIKE '%${search}%' or
                                                      id IN (${komitentsIds.toString()}) and povrsina LIKE '%${search}%' or
                                                      id IN (${komitentsIds.toString()}) and dogovorenaKolicina LIKE '%${search}%';
                        `,
                    { replacements: { status: 'active' }, type: sequelize.QueryTypes.SELECT }
                );
                komitentsIds = searchedKomitenti.map(s => s.id);
            }

            const komitentiDb = await Komitent.findAll({
                where: {
                    id: komitentsIds
                },
                include: [{
                    model: Komitent_Prasalnik_Mart,
                    required: true,
                },
                {
                    model: Komitent_Prasalnik_Maj,
                    required: true,
                },
                {
                    model: Komitent_Prasalnik_Juni,
                    required: true,
                },
                {
                    model: Komitent_Prasalnik_Avgust,
                    required: true,
                }]
            });

            const komitentiAsync = komitentiDb.reduce(async (acc, dbkomitent) => {

                const prasalnik_Mart = await Prasalnik_Mart.findOne({
                    where: {
                        id: dbkomitent.Komitent_Prasalnik_Mart.prasalnikId
                    }
                });
                const prasalnik_Maj = await Prasalnik_Maj.findOne({
                    where: {
                        id: dbkomitent.Komitent_Prasalnik_Maj.prasalnikId
                    }
                });
                const prasalnik_Juni = await Prasalnik_Juni.findOne({
                    where: {
                        id: dbkomitent.Komitent_Prasalnik_Juni.prasalnikId
                    }
                });
                const prasalnik_Avgust = await Prasalnik_Avgust.findOne({
                    where: {
                        id: dbkomitent.Komitent_Prasalnik_Avgust.prasalnikId
                    }
                });

                const komitent = {
                    ...dbkomitent.dataValues,
                    prasalnici: {
                        prasalnik_Mart: {
                            status: prasalnik_Mart.status,
                            popolneto: prasalnik_Mart.popolneto,
                        },
                        prasalnik_Maj: {
                            status: prasalnik_Maj.status,
                            popolneto: prasalnik_Maj.popolneto,
                        },
                        prasalnik_Juni: {
                            status: prasalnik_Juni.status,
                            popolneto: prasalnik_Juni.popolneto,
                        },
                        prasalnik_Avgust: {
                            status: prasalnik_Avgust.status,
                            popolneto: prasalnik_Avgust.popolneto,
                        }
                    }
                };
                acc = await acc;
                acc.push(komitent);

                return acc;
            }, []);

            const komitenti = await komitentiAsync;

            return res.status(200).json({ komitenti });
        } catch (err) {
            console.log(err);
            return res.status(500).json({ msg: 'Internal server error' });
        }
    }

    const removeKomitentiFromtehnichar = async (req, res) => {

        const { tehnichariIds, komitentiIds } = req.body;

        try {

            if (!!!tehnichariIds.length || !!!komitentiIds.length) {
                return res.status(400).json({ msg: 'Bad Request' });
            }

            await TehnicharKomitent.destroy({
                where: {
                    tehnicharId: tehnichariIds[0],
                    komitentId: komitentiIds,
                },
            });

            await Komitent.update({
                reonski: null
            }, {
                where: {
                    id: komitentiIds
                },
            });

            return res.status(200).json({ msg: 'Success' });
        } catch (err) {
            console.log(err);
            return res.status(500).json({ msg: 'Internal server error' });
        }

    };

    const prashalniciCharts = async (req, res) => {

        try {

            const prasalnik_Mart = await Prasalnik_Mart.findAll();
            const zatvoren_Prasalnik_Mart = prasalnik_Mart.filter(p => p.status === 2);
            const avarageZatvoreniPrasalnikMart = Math.ceil(zatvoren_Prasalnik_Mart.length * 100 / prasalnik_Mart.length);

            const prasalnik_Maj = await Prasalnik_Maj.findAll();
            const zatvoren_Prasalnik_Maj = prasalnik_Maj.filter(p => p.status === 2);
            const avarageZatvoreniPrasalnikMaj = Math.ceil(zatvoren_Prasalnik_Maj.length * 100 / prasalnik_Maj.length);


            const prasalnik_Juni = await Prasalnik_Juni.findAll();
            const zatvoren_Prasalnik_Juni = prasalnik_Juni.filter(p => p.status === 2);
            const avarageZatvoreniPrasalnikJuni = Math.ceil(zatvoren_Prasalnik_Juni.length * 100 / prasalnik_Juni.length);


            const prasalnik_Avgust = await Prasalnik_Avgust.findAll();
            const zatvoren_Prasalnik_Avgust = prasalnik_Avgust.filter(p => p.status === 2);
            const avarageZatvoreniPrasalnikAvgust = Math.ceil(zatvoren_Prasalnik_Avgust.length * 100 / prasalnik_Avgust.length);

            return res.status(200).json({
                charts: [
                    {
                        zatvoreniProcent: avarageZatvoreniPrasalnikMart,
                        otvoreniProcent: 100 - avarageZatvoreniPrasalnikMart,
                        name: 'Март'
                    },
                    {
                        zatvoreniProcent: avarageZatvoreniPrasalnikMaj,
                        otvoreniProcent: 100 - avarageZatvoreniPrasalnikMaj,
                        name: 'Мај'
                    },
                    {
                        zatvoreniProcent: avarageZatvoreniPrasalnikJuni,
                        otvoreniProcent: 100 - avarageZatvoreniPrasalnikJuni,
                        name: 'Јуни'
                    },
                    {
                        zatvoreniProcent: avarageZatvoreniPrasalnikAvgust,
                        otvoreniProcent: 100 - avarageZatvoreniPrasalnikAvgust,
                        name: 'Август'
                    },
                ]
            });

        } catch (err) {
            console.log(err);
            return res.status(500).json({ msg: 'Internal server error' });
        }

    };

    return {
        komitenti,
        createKomitent,
        getKomitentById,
        changeKomitentProfile,
        deleteKomitenti,

        getPrasalnikMartById,
        getPrasalnikMajById,
        getPrasalnikJuniById,
        getPrasalnikAvgustById,

        changePrasalnikMartById,
        changePrasalnikMajById,
        changePrasalnikJuniById,
        changePrasalnikAvgustById,

        getTehnichari,
        addKomitentiToTehnichari,
        getKomitentiByTehnicharId,
        getKomitentiMobile,
        removeKomitentiFromtehnichar,
        prashalniciCharts,
    };
};

module.exports = KomitentController;
