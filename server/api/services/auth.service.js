const jwt = require('jsonwebtoken');

const secret = 'secret123123';
//process.env.NODE_ENV === 'production' ? process.env.JWT_SECRET :

const authService = () => {
  const issue = (payload) => jwt.sign(payload, secret, { expiresIn: "1y" });
  const issueForgot = (payload) => jwt.sign(payload, secret, { expiresIn: "1y" });
  const verify = (token, cb) => jwt.verify(token, secret, {}, cb);
  const resetPasswordUserId = (token) => { return jwt.verify(token, secret).id };

  const getUserIdByToken = (token) => {
    const parts = token.split(' ');
    const tokenToVerify = parts[1];
    return jwt.verify(tokenToVerify, secret).id;
  };

  const getTokenProps = (token) => { return jwt.verify(token, secret) };

  return {
    issue,
    verify,
    getUserIdByToken,
    issueForgot,
    resetPasswordUserId,
    getTokenProps,
  };
};

module.exports = authService;
